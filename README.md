# Sora Development Website

This repository hosts the code for the website of the Development Branch of Sora Technologies, LLC.

## Site Outline

### Home

* Should be more graphical. High-level descriptions of what we do, how we do it, and images to capture the eye.
* Why we do what we do

### About

* Describes the company.
* Who are we
   * Jake
   * Jerad
   * Andrew
   * Todd
* What we do

### Work
* A showcase of our previous work. Screenshots and descriptions.
   * High-level description of each project
   * Screenshots
   * List of featured technologies
* Mention our Code Ownership philosophy
   * "We write it, you own it"
* Mention our dedication to using Open Source tools
   * Open source means ongoing support and development
   * No licensing fees

### Services
* List of things we can do for clients
   * Bespoke web application development
   * Web Hosting
   * API Development
   * Database Implementation Consulting
   * _SEO*_
   * _iOS app development*_
   * _Progressive Web Apps*_
   * _Website Design and Development*_

   _* Need more examples of these before putting them on site_

### Contact
* Email Address
  * Could use dev@soratech.com
  * It would be better to use an @soratech.dev address
* Phone Number
  * Sales Line
* Google Map with Office Location


## Work

_Which projects to showcase_

* Kintrip (Need permission from **AWTY**)
  * Website
  * iOS App
* Ops Boards (Need permission from **MORD**)
  * Boards
  * Datafinder API
  * Asset Management API
* Sora Status Boards
  * Stat Board
  * Reports
  * Client Reporting API
* Bender
  * Market as either __Slackbot__ or __Chatbot__
* Time Tracker (Need permission from **ENCO**)
  * Need login to get screenshots for this
     