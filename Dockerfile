FROM alpine:latest

COPY devWebsite /var/www/devWebsite/devWebsite

ENTRYPOINT [ "/var/www/devWebsite/devWebsite" ]