document.addEventListener('DOMContentLoaded', () => {

    const sections = document.getElementsByTagName('section');
    const observer = new IntersectionObserver(entries => {
        for (let entry of entries) {
            if (entry.intersectionRatio > 0) {
                const id = entry.target.id;
                const link = document.querySelector(`nav a[href="#${id}"]`);
                const navDiv = document.querySelector('nav div');
                document.querySelector('nav .active').classList.remove('active');
                link.classList.add('active');

                if (id === 'home') {
                    navDiv.removeAttribute('style');
                } else {
                    if (!navDiv.hasAttribute('style')) {
                        navDiv.style.display = 'flex';
                    }
                }
            }
        }
    }, { threshold: [0.9], rootMargin: '-40px 0px 0px 0px' });

    for (let section of sections) {
        observer.observe(section);
    }

    const workButtons = document.querySelectorAll('#exampleTabs button');
    for (let workButton of workButtons) {
        workButton.addEventListener('click', e => {
            updateWork(e.currentTarget.dataset.name);
        });
    }

    document.querySelector('nav a[href="#contact"]').addEventListener('click', e => {
        const link = e.currentTarget;
        setTimeout(() => {
            document.querySelector('nav .active').classList.remove('active');
            link.classList.add('active');
        }, 1000);
    });

    document.getElementById('hamburger').addEventListener('click', e => {
        const button = e.currentTarget;
        const ul = button.querySelector('ul');

        if (ul.offsetHeight == 0) {
            ul.style.display = 'block';
        } else {
            ul.style.display = 'none';
        }
    });

    document.getElementById('exampleSelect').addEventListener('change', e => {
        updateWork(e.currentTarget.value);
    });

    document.querySelector('nav div').addEventListener('click', e => {
        window.scrollTo(0, 0);
    });
});

function updateWork(selected) {
    const picture = document.querySelector('#screenshots img');
    const title = document.querySelector('#description .title');
    const tagList = document.querySelector('#techTags ul');
    const currentArticle = document.querySelector('#description article.visible');
    let tags = [];

    switch (selected) {
        case 'kintrip':
            picture.src = 'img/kintrip.png';
            title.textContent = 'Kintrip';
            tags = ['PHP', 'Yii2', 'JavaScript', 'AWS', 'SASS'];
            break;
        case 'mordue':
            picture.src = 'img/opsBoard.png';
            title.textContent = 'Mordue Ops Board';
            tags = ['PHP', 'Yii2', 'JavaScript', 'Go', 'REST'];
            break;
        case '360Yield':
            picture.src = 'img/timeTracker.png';
            title.textContent = 'Time Tracker';
            tags = ['PHP', 'Yii2', 'SQL'];
            break;
        case 'slackbot':
            picture.src = 'img/bender.png';
            title.textContent = 'Chat Bot';
            tags = ['Go', 'REST', 'Azure', 'Slack'];
            break;
        case 'statusboard':
            picture.src = 'img/statusboard.png';
            title.textContent = 'Internal Statusboard';
            tags = ['Go', 'MongoDB', 'JavaScript', 'REST'];
            break;
    }

    currentArticle.classList.remove('visible');
    document.querySelector(`#description article[data-name="${selected}"]`).classList.add('visible');

    while (tagList.firstChild) {
        tagList.removeChild(tagList.firstChild);
    }

    for (let tag of tags) {
        const li = document.createElement('li');
        li.appendChild(document.createTextNode(tag));
        tagList.appendChild(li);
    }
}
